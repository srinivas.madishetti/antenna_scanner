import os
import time
import readchar
import re
import random
import pyvisa as visa    #PyVisa is required along with NIVisa
import skrf as rf
from datetime import datetime
from matplotlib import pyplot as plt
from colorama import Fore, Style
import numpy as np
from googletrans import Translator
import pandas as pd
translator = Translator()
di = {i: translator.translate(i, dest='vi').text for i in ('Waiting for QR', 'Pass', 'Fail', 'Detected QR Code', 
'Unit Passed, Move on to next', 'Invalid QR Code', 'Status', 'Failure to connect to VNA', 'Check network settings')}
import threading
import queue,time
import ftplib


myQ=queue.Queue()

def readcharIntoQ():
    # function runs in a separate thread and puts all input into a q
    while True:
        a=readchar.readchar()
        myQ.put(a)
        
def flush():
    extra=''
    while True:
        try:
            while True:
                extra+=myQ.get_nowait()
        except queue.Empty:
            break
    return extra 

class RF_Testing():
    
    def __init__(self):
        self.rm = visa.ResourceManager()
        try:
            self.CMT = self.rm.open_resource('TCPIP0::localhost::5025::SOCKET')

        except Exception as e:
            print(e)

            print(rf"Failure to connect to VNA({di['Failure to connect to VNA']})")

            print(rf"Check network settings({di['Check network settings']})")

        #The VNA ends each line with this. Reads will time out without this

        self.CMT.read_termination='\n'

        #Set a really long timeout period for slow sweeps

        self.CMT.timeout = 10000
        
    @staticmethod
    def scan_qr():
        print(fr'Waiting for QR({di["Waiting for QR"]}): ')
        barcodeLength=18
        barcode=''
        for i in range(barcodeLength):
            barcode +=myQ.get()
        time.sleep(0.1)
        return barcode
        #return ''.join([readchar.readkey() for i in range(18)])
        #return "VEXOS2121-00000001"
        # print(f'Waiting for QR({di["Waiting for QR"]}): ')
        # return ''.join([readchar.readkey() for i in range(18)])
        # #return "VEXOS2121-00000001"
    
    @staticmethod
    def validate_qr(qr):
        '''
        '''
        #sample_qr = 'VEXOSWWYY-00000001'
        patrn = r"[A-Z]{5}[0-9]{4}\-[0-9]{8}"
        return re.match(patrn, qr)

    def plotting(self):

        #files_list = os.listdir(rf"C:\VNA\smp_cable_tcycle\{self.qr}/")
        #ntwk = rf.Network(f'C:\VNA\smp_cable_tcycle\{self.qr}/'+files_list[0])
        # ntwk = rf.Network(r'C:/\Users/\manikanth.patwari/\Downloads/\sn5_fixed1_chamber2_1.s2p')
        ntwk = rf.Network(f'{self.dt_string}'+'.s2p')
        # ntwk.s = 20*np.log10(np.abs(ntwk.s))
        self.s11_db = 20*np.log10(np.abs(ntwk.s[:,0,0]))
        self.s21_db = 20*np.log10(np.abs(ntwk.s[:,1,0]))
        self.s22_db = 20*np.log10(np.abs(ntwk.s[:,1,1]))
       
        path = "s_parameters.xlsx"

        
        dataframe1 = pd.read_excel(path)
        
        s11_l = dataframe1['S11_lower']
        s11_u = dataframe1['S11_upper']
        s21_l = dataframe1['S21_lower']
        s21_u = dataframe1['S21_upper']
        s22_l = dataframe1['S22_lower']
        s22_u = dataframe1['S22_upper']
        
     
        
        self.s11_lower = np.array([s11_l[i] for i in range(len(self.s11_db))])
        self.s11_upper = np.array([s11_u[i] for i in range(len(self.s11_db))])

        self.s21_lower = np.array([s21_l[i] for i in range(len(self.s21_db))])
        self.s21_upper = np.array([s21_u[i] for i in range(len(self.s21_db))])

        self.s22_lower = np.array([s22_l[i] for i in range(len(self.s22_db))])
        self.s22_upper = np.array([s22_u[i] for i in range(len(self.s22_db))])

        # self.s11_lower = np.array([-14 for _ in self.s11_db])
        # self.s11_upper = np.array([-7 for _ in self.s11_db])

        # self.s21_lower = np.array([-45 for _ in self.s21_db])
        # self.s21_upper = np.array([-27 for _ in self.s21_db])

        # self.s22_lower = np.array([-15 for _ in self.s22_db])
        # self.s22_upper = np.array([-8 for _ in self.s22_db])
        
        freq = ntwk.frequency.f / 10**6
        
        plt.plot(freq, self.s11_db, color='green')
        plt.plot(freq, self.s21_db, color='black')
        plt.plot(freq, self.s22_db, color='red')
        # import pdb;pdb.set_trace()
        freq_list = list(freq)
        try:
            self.lower_bound = freq_list.index(700.0)
            self.upper_bound = freq_list.index(960.0)
        except ValueError as e:
            self.lower_bound = 201
            self.upper_bound = 721            
        
        plt.fill_between(freq[self.lower_bound:self.upper_bound], self.s11_lower[self.lower_bound:self.upper_bound], self.s11_upper[self.lower_bound:self.upper_bound], color='green', alpha=0.1, label='S11')
        plt.fill_between(freq[self.lower_bound:self.upper_bound], self.s21_lower[self.lower_bound:self.upper_bound], self.s21_upper[self.lower_bound:self.upper_bound], color='grey', alpha=0.1, label='S21')
        plt.fill_between(freq[self.lower_bound:self.upper_bound], self.s22_lower[self.lower_bound:self.upper_bound], self.s22_upper[self.lower_bound:self.upper_bound], color='red', alpha=0.1, label='S22')
        plt.xlabel("Frequency (MHz)")
        plt.ylabel("dB")
        plt.grid(True)
        
        plt.legend(['S11', 'S21', 'S22'])
        
        #figManager = plt.get_current_fig_manager()
        #figManager.window.showMaximized()
        
        #mng = plt.get_current_fig_manager()
        #mng.frame.Maximize(True)

        #mng = plt.get_current_fig_manager()
        ### works on Ubuntu??? >> did NOT working on windows
        # mng.resize(*mng.window.maxsize())
        #mng.window.state('zoomed')
        
        figure = plt.gcf()  # get current figure
        figure.set_size_inches(32, 18) # set figure's size manually to your full screen (32x18)
        #plt.savefig('filename.png', bbox_inches='tight') # bbox_inches removes extra white spaces

        self.timestamp = self.dt_string.split('\\')[-1].split('.')[0]
        plt.savefig(fr"C:\VNA\smp_cable_tcycle\{self.qr}\{self.timestamp}.png", bbox_inches='tight', dpi=100)
        
        self.status = self.validate_data()
        print(fr'Status ({di["Status"]})  : ' + (Fore.GREEN + f'Pass ({di["Pass"]})') if self.status == 'Pass' else Fore.RED + f'Fail ({di["Fail"]})')
        # print('Status  : ' + (Fore.GREEN + di['Pass']) if self.status == 'Pass' else Fore.RED + di['Fail'])
        print(Style.RESET_ALL)

        if self.status == 'Fail':
            print(fr'Status ({di["Status"]})  : ' + (Fore.GREEN + f'Pass ({di["Pass"]})') if self.status == 'Pass' else Fore.RED + f'Please try re-installing and re-measuring the unit ({di["Fail"]})')
            print(fr'Status ({di["Status"]})  : ' + (Fore.GREEN + f'Pass ({di["Pass"]})') if self.status == 'Pass' else Fore.RED + f'Please close the pop-up graphics to continue ({di["Fail"]})')
            
            plt.show()
        else:
            print(fr"unit passes, move on to next ({di['Unit Passed, Move on to next']})")
        #######################################################Subplotting#############################################################################
        fig, axs = plt.subplots(2, 2, constrained_layout=True)
        
        axs[0, 0].plot(freq, self.s11_db, color='green')
        axs[0, 0].plot(freq, self.s21_db, color='black')
        axs[0, 0].plot(freq, self.s22_db, color='red')
        axs[0, 0].set_title('All')
        axs[0, 0].fill_between(freq[self.lower_bound:self.upper_bound], self.s11_lower[self.lower_bound:self.upper_bound], self.s11_upper[self.lower_bound:self.upper_bound], color='green', alpha=0.1, label='S11')
        axs[0, 0].fill_between(freq[self.lower_bound:self.upper_bound], self.s21_lower[self.lower_bound:self.upper_bound], self.s21_upper[self.lower_bound:self.upper_bound], color='black', alpha=0.1, label='S21')
        axs[0, 0].fill_between(freq[self.lower_bound:self.upper_bound], self.s22_lower[self.lower_bound:self.upper_bound], self.s22_upper[self.lower_bound:self.upper_bound], color='red', alpha=0.1, label='S22')
        axs[0, 0].grid()
        axs[0, 0].legend(['S11', 'S21', 'S22'])

        axs[0, 1].plot(freq, self.s11_db, color='green')
        axs[0, 1].set_title('S11')
        axs[0, 1].fill_between(freq[self.lower_bound:self.upper_bound], self.s11_lower[self.lower_bound:self.upper_bound], self.s11_upper[self.lower_bound:self.upper_bound], color='green', alpha=0.1, label='S11')
        axs[0, 1].grid()
        axs[0, 1].legend(['S11'])
        


        axs[1, 0].plot(freq, self.s21_db, color='black')
        axs[1, 0].set_title('S21')
        axs[1, 0].fill_between(freq[self.lower_bound:self.upper_bound], self.s21_lower[self.lower_bound:self.upper_bound], self.s21_upper[self.lower_bound:self.upper_bound], color='black', alpha=0.1, label='S21')
        axs[1, 0].grid()
        axs[1, 0].legend(['S21'])

        axs[1, 1].plot(freq, self.s22_db, color='red')
        axs[1, 1].set_title('S22')
        axs[1, 1].fill_between(freq[self.lower_bound:self.upper_bound], self.s22_lower[self.lower_bound:self.upper_bound], self.s22_upper[self.lower_bound:self.upper_bound], color='red', alpha=0.1, label='S22')
        axs[1, 1].grid()
        axs[1, 1].legend(['S22'])
        


        for ax in axs.flat:
            ax.set(xlabel='Frequency(MHz)', ylabel='dB')

        # Hide x labels and tick labels for top plots and y ticks for right plots.
        # for ax in axs.flat:
        #     ax.label_outer()
        
        figure = plt.gcf()  # get current figure
        figure.set_size_inches(32, 18) # set figure's size manually to your full screen (32x18)
        #plt.savefig('filename.png', bbox_inches='tight') # bbox_inches removes extra white spaces

        #self.timestamp = self.dt_string.split('\\')[-1].split('.')[0]
        plt.savefig(fr"C:\VNA\smp_cable_tcycle\{self.qr}\{self.timestamp}_Subplots.png", bbox_inches='tight', dpi=100)
        
        #fig.savefig(fr"C:\VNA\smp_cable_tcycle\{self.qr}\{self.timestamp}_Subplots.jpeg")
        plt.clf()
        plt.close()
        # plt.legend().remove()
        
    #############################################################################################################

    def validate_data(self):
        '''
        '''
        s11_comp = self.s11_lower[self.lower_bound:self.upper_bound] < self.s11_db[self.lower_bound:self.upper_bound]
        s11_comp_1 = self.s11_db[self.lower_bound:self.upper_bound] < self.s11_upper[self.lower_bound:self.upper_bound]
        s21_comp = self.s21_lower[self.lower_bound:self.upper_bound] < self.s21_db[self.lower_bound:self.upper_bound]
        s21_comp_1 = self.s21_db[self.lower_bound:self.upper_bound] < self.s21_upper[self.lower_bound:self.upper_bound]
        s22_comp = self.s22_lower[self.lower_bound:self.upper_bound] < self.s22_db[self.lower_bound:self.upper_bound]
        s22_comp_1 = self.s22_db[self.lower_bound:self.upper_bound] < self.s22_upper[self.lower_bound:self.upper_bound]
        count = 0
        for i in (s11_comp, s11_comp_1, s21_comp, s21_comp_1, s22_comp, s22_comp_1):
            if False in i:
                count += 1
                return 'Fail'
        if not count:
            return 'Pass'
        

    def start_loop(self):
        t1 = threading.Thread(target=readcharIntoQ)
        t1.daemon=True #used to kill thread when program ends
        t1.start()
        self.lst_of_antenas = []
        self.status = 'Pass'
        while self.status:
            files_list = []
            flush()
            self.qr = RF_Testing.scan_qr()
            print(f'barcode={self.qr}, extra={flush()}')
            if RF_Testing.validate_qr(self.qr):
                start_time=time.time()
                now = datetime.now()
                #Code to Clear the folder.
                files_list = []
                try:
                    files_list = os.listdir(fr'C:\\VNA\\smp_cable_tcycle\\{self.qr}')
                except FileNotFoundError as e:
                    pass
                if files_list:
                    print('Detected already used antenna. New results will be augmented')
                    # for i in files_list:
                    #     os.remove(f'C:\\VNA\\smp_cable_tcycle\\{self.qr}\\{i}')
                
                self.dt_string = now.strftime(f"C:\\VNA\\smp_cable_tcycle\\{self.qr}\\ %d-%m-%Y-%H-%M-%S") #read current date and time

                self.CMT.write('MMEM:STOR:SNP:TYPE:S2P') #select sNp file type

                self.CMT.write('MMEM:STOR:SNP '+self.dt_string+'\n') #save touchstone file

                time.sleep(1) #sleep for N secs

                stop_time=time.time() #stop timer

                elapsed_time=(stop_time-start_time)/60 #calculate time elapsed in minutes

                print('Elapsed time in minutes: '+ str(elapsed_time))
                self.lst_of_antenas.append(self.qr)
                print(Fore.YELLOW + f"Detected QR Code({di['Detected QR Code']}): " + self.qr)
                # print(Fore.YELLOW + di["Detected QR Code"]+": " + self.qr)
                print(Style.RESET_ALL)
                self.plotting()
                with open(rf"C:\VNA\smp_cable_tcycle\{self.qr}\{self.qr}_status.txt", 'a+') as fp:
                    fp.write(f"{self.timestamp} :: {self.status}\n")
                with open("AntennaIndex.txt", 'a+') as fp:
                    fp.write(f"{self.timestamp} :: {self.qr}\n")
                
                #file storage online
                #session = ftplib.FTP('vexosems.files.com','AST2023','P@ssp0rt2o23')
                try:
                    session.mkd(f'{self.qr}')
                except:
                    time.sleep(0.1)
                
                session.cwd(f'{self.qr}')
                file = open(fr"C:\VNA\smp_cable_tcycle\{self.qr}\{self.timestamp}.png",'rb')                  # file to send
                session.storbinary(f'STOR {self.timestamp}.png', file)     # send the main plot
                file.close()                                    # close file and FTP
                file = open(f'{self.dt_string}'+'.s2p','rb')                  # file to send
                session.storbinary(f'STOR {self.timestamp}'+'.s2p', file)     # send the s2p file
                file.close()  
                file = open(fr"C:\VNA\smp_cable_tcycle\{self.qr}\{self.timestamp}_Subplots.png",'rb')                  # file to send
                session.storbinary(f'STOR {self.timestamp}_Subplots.png', file)     # send the subplot
                file.close()  
                file = open(rf"C:\VNA\smp_cable_tcycle\{self.qr}\{self.qr}_status.txt",'rb')                  # file to send
                session.storbinary(f'STOR {self.qr}_status.txt', file)     # send the status
                file.close()  
                session.cwd('../')
                #session.quit()

            else:
                print(fr'Invalid QR Code ({di["Invalid QR Code"]})', self.qr)
                
obj = RF_Testing()
session = ftplib.FTP('vexosems.files.com','AST2023','P@ssp0rt2o23')
session.cwd('Vexos') 
session.cwd('customers') 
session.cwd('AST')
session.cwd('K') 
obj.start_loop()
session.quit()
#session.quit()